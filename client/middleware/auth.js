export default async ({ store, route, redirect, $axios }) => {
  console.log(`Processing "${route.fullPath}" in middleware/auth.js`);

	//TODO: Allow access to all main pages!
	if (route.fullPath === '/') {
		console.log(`Page with anonymous access!`);
		return;
	}

	// Use info derived by token from global vuex store
	if (!store.getters.isAuthenticated) {
		console.log(`You are not logged in! Redirecting to safe zone`);
		return redirect('/');
	}

	let res = await $axios({
		method:"POST",
		url: `/rpc/autorizar`,
		headers: {
			Authorization:'Bearer ' + store.state.token
		},
		data: {caminho_servico: route.fullPath }
	})
	
	if(res.data === false) {
		console.log('You are not allowed to access this page. Redirecting')
		return redirect('/')
	}
}
