import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
})

const initialState =  {
  token: '',
  aluno: '',
}

const initialSharedState = {
  id_pessoa: -1,
  planoSelected: -1,
  disciplinasPlanoSelected: [],
  disciplinasFeitas: [],
  disciplinasFazendo: [],
  semestres: [1,2,3,4,5,6,7,8]
}

export default () => {
  return new Vuex.Store({
    plugins: [vuexLocal.plugin],

    state: {
      shared_state: { id_pessoa: initialSharedState.id_pessoa,
                      planoSelected: initialSharedState.planoSelected,
                      disciplinasFeitas: initialSharedState.disciplinasFeitas,
                      disciplinasFazendo: initialSharedState.disciplinasFazendo,
                      disciplinasPlanoSelected: initialSharedState.disciplinasPlanoSelected,
                      semestres: initialSharedState.semestres
                    },
      token: initialState.token,
      aluno: initialState.aluno,
    },

    getters: {
      isAuthenticated(state) {
        return state.token !== '';
      },

      getIdPessoa(state) {
        return state.shared_state.id_pessoa;
      },

      getDisciplinasFeitas(state) {
        return state.shared_state.disciplinasFeitas;
      },

      getDisciplinasFazendo(state) {
        return state.shared_state.disciplinasFazendo;
      },

      getDisciplinasPlanoSelected(state) {
        return state.shared_state.disciplinasPlanoSelected;
      },

      getPlanoSelected(state) {
        return state.shared_state.planoSelected;
      },
        
      getAluno(state) {
        return state.aluno;
      },

      getSemestres(state) {
        return state.shared_state.semestres;
      }
    },

    mutations: {

      addSemestre(state) {
        if(state.shared_state.semestres.length < 12)
          state.shared_state.semestres.push(state.shared_state.semestres.length + 1);
      },
      
      setPessoa(state, { id_pessoa }) {
        state.shared_state.id_pessoa = id_pessoa;
      },

      setToken(state, { token }) {
        state.token = token;
      },

      setPlanSelected(state, { planSelected }) {
        state.planSelected = planSelected;
      },

      setDisciplinasFeitas(state, { disciplinasFeitas }) {
        state.shared_state.disciplinasFeitas = disciplinasFeitas;
      },

      setDisciplinasFazendo(state, { disciplinasFazendo }) {
        state.shared_state.disciplinasFazendo = disciplinasFazendo;
      },

      setPlanoSelected(state, {planoSelected, disciplinasPlanoSelected}) {
        state.shared_state.planoSelected = planoSelected;
        state.shared_state.disciplinasPlanoSelected = disciplinasPlanoSelected;
      },

      removeDisciplinaPlano(state, { plano, disciplina }) {
        if(this.state.shared_state.planoSelected === plano) 
          state.shared_state.disciplinasPlanoSelected = state.shared_state.disciplinasPlanoSelected.filter(el => el.id_disciplina !== disciplina.id_disciplina)
      },

      addDisciplinaPlano(state, { plano, disciplina }) {
        if(this.state.shared_state.planoSelected === plano) 
          state.shared_state.disciplinasPlanoSelected.push(disciplina)
      },



      setAluno(state, { aluno }){
        state.aluno = aluno;
      },

      resetState(state) {
        
        Object.assign(state, initialState);
        initialSharedState.disciplinasFazendo = [];
        initialSharedState.disciplinasFeitas = [];
        initialSharedState.disciplinasPlanoSelected = [];
        initialSharedState.semestres = [1, 2, 3, 4, 5, 6, 7, 8];
        Object.assign(state.shared_state, initialSharedState);
      }
    }
  });
}
