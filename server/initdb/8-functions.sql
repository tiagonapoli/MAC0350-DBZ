CREATE OR REPLACE FUNCTION
public.disciplinas_do_modulo(id_do_modulo integer)
  RETURNS TABLE(id_disciplina bigint,
                codigo_disciplina  character(7),
                creditos_aula      integer,
                creditos_trabalho  integer,
                carga_horaria      integer,
                ativacao           date,
                desativacao        date,
                nome               varchar(280),
                ementa             text)
  LANGUAGE SQL
AS $$
  SELECT d
  FROM public.disciplinas as d,
       public.optativas_compoem_modulos as rel
  WHERE rel.id_disciplina = d.id_disciplina 
        AND rel.id_modulo = id_do_modulo;
$$;

-------------------------------------------------

CREATE OR REPLACE FUNCTION
public.get_id_pessoa(email varchar(256))
  RETURNS TABLE(id_pessoa bigint)
  LANGUAGE SQL
AS $$
  SELECT rel.id_pessoa
  FROM 
      auth.usuarios as u,
      public.pessoas_geram_usuarios as rel
  WHERE u.email_usuario = email AND
        u.id_usuario = rel.id_usuario
$$;


-------------------------------------------------

CREATE OR REPLACE FUNCTION
public.disciplinas_cursadas_pelo_aluno(arg_id_pessoa integer)
  RETURNS TABLE(id_disciplina bigint,
                codigo_disciplina  character(7),
                creditos_aula      integer,
                creditos_trabalho  integer,
                carga_horaria      integer,
                nome               varchar(280),
                ano_semestre       numeric(5, 1),
                nota               numeric(3, 1)
)
  LANGUAGE SQL
AS $$
  SELECT di.id_disciplina, di.codigo_disciplina, di.creditos_aula,
         di.creditos_trabalho, di.carga_horaria, di.nome, of.ano_semestre, cursa.nota
  FROM public.disciplinas as di,
       public.professores_oferecem_disciplinas as of,
       public.alunos_cursam_disciplinas as cursa
  WHERE di.id_disciplina = of.id_disciplina AND
        cursa.id_oferecimento = of.id_oferecimento AND
        cursa.id_pessoa = arg_id_pessoa;
$$;

-----------------------------------------------------
CREATE OR REPLACE FUNCTION
public.disciplinas_planejadas_pelo_aluno(arg_id_pessoa integer)
  RETURNS TABLE(id_disciplina bigint,
                codigo_disciplina  character(7),
                creditos_aula      integer,
                creditos_trabalho  integer,
                carga_horaria      integer,
                nome               varchar(280),
                num_plano          bigint,
                id_pessoa          bigint,
                nusp_aluno         varchar(10),
                ano_semestre       numeric(5, 1),
                ementa             text
)
  LANGUAGE SQL
AS $$
  SELECT di.id_disciplina, di.codigo_disciplina, di.creditos_aula, di.creditos_trabalho, di.carga_horaria,
         di.nome, plan.num_plano, plan.id_pessoa, plan.nusp_aluno, plan.ano_semestre, di.ementa
  FROM public.alunos_planejam_disciplinas as plan,
       public.disciplinas as di
  WHERE di.id_disciplina = plan.id_disciplina AND
        plan.id_pessoa = arg_id_pessoa;
$$;

----------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.prerrequisitos_disciplina(arg_id_disciplina integer)
  RETURNS TABLE(id_disciplina bigint,
                codigo_disciplina  character(7),
                nome               varchar(280)
)
  LANGUAGE SQL
AS $$
  SELECT di.id_disciplina, di.codigo_disciplina, di.nome
  FROM public.prerequisitos as pre,
       public.disciplinas as di
  WHERE di.id_disciplina = pre.id_prerequisito AND
        pre.id_disciplina = arg_id_disciplina
$$;

-----------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.disciplinas_planejadas_pelo_aluno_no_plano(arg_id_pessoa integer, arg_num_plano integer)
  RETURNS TABLE(id_disciplina bigint,
                codigo_disciplina  character(7),
                creditos_aula      integer,
                creditos_trabalho  integer,
                carga_horaria      integer,
                nome               varchar(280),
                num_plano          bigint,
                id_pessoa          bigint,
                nusp_aluno         varchar(10),
                ano_semestre       numeric(5, 1),
                ementa             text
)
  LANGUAGE SQL
AS $$
  SELECT di.id_disciplina, di.codigo_disciplina, di.creditos_aula, di.creditos_trabalho, di.carga_horaria,
         di.nome, plan.num_plano, plan.id_pessoa, plan.nusp_aluno, plan.ano_semestre, di.ementa
  FROM public.alunos_planejam_disciplinas as plan,
       public.disciplinas as di
  WHERE di.id_disciplina = plan.id_disciplina AND
        plan.id_pessoa = arg_id_pessoa AND
		plan.num_plano = arg_num_plano;
$$;

--------------------------------------------------------------

CREATE OR REPLACE FUNCTION
public.disciplina_planejada_pelo_aluno_no_plano(arg_id_pessoa integer, arg_num_plano integer, arg_id_disciplina integer)
  RETURNS TABLE(id_disciplina bigint,
                codigo_disciplina  character(7),
                creditos_aula      integer,
                creditos_trabalho  integer,
                carga_horaria      integer,
                nome               varchar(280),
                num_plano          bigint,
                id_pessoa          bigint,
                nusp_aluno         varchar(10),
                ano_semestre       numeric(5, 1),
                ementa             text
)
  LANGUAGE SQL
AS $$
  SELECT di.id_disciplina, di.codigo_disciplina, di.creditos_aula, di.creditos_trabalho, di.carga_horaria,
         di.nome, plan.num_plano, plan.id_pessoa, plan.nusp_aluno, plan.ano_semestre, di.ementa
  FROM public.alunos_planejam_disciplinas as plan,
       public.disciplinas as di
  WHERE di.id_disciplina = plan.id_disciplina AND
        plan.id_pessoa = arg_id_pessoa AND
		plan.num_plano = arg_num_plano AND
		plan.id_disciplina = arg_id_disciplina;
$$;

-----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION
public.disciplinas_obrigatorias()
  RETURNS TABLE(id_disciplina bigint,
                codigo_disciplina  character(7),
                creditos_aula      integer,
                creditos_trabalho  integer,
                carga_horaria      integer,
                ativacao           date,
                desativacao        date,
                nome               varchar(280),
                ementa             text)
  LANGUAGE SQL
AS $$
  SELECT d
  FROM public.disciplinas as d,
       public.grade_obrigatoria as rel
  WHERE rel.id_disciplina = d.id_disciplina;
$$;

-----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION
public.disciplinas_eletivas()
  RETURNS TABLE(id_disciplina bigint,
                codigo_disciplina  character(7),
                creditos_aula      integer,
                creditos_trabalho  integer,
                carga_horaria      integer,
                ativacao           date,
                desativacao        date,
                nome               varchar(280),
                ementa             text)
  LANGUAGE SQL
AS $$
  SELECT d
  FROM public.disciplinas as d,
       public.grade_optativa as rel
  WHERE rel.id_disciplina = d.id_disciplina AND
  		rel.eletiva= TRUE;
$$;

-----------------------------------------------------------------------
CREATE OR REPLACE FUNCTION
public.disciplinas_livres()
  RETURNS TABLE(id_disciplina bigint,
                codigo_disciplina  character(7),
                creditos_aula      integer,
                creditos_trabalho  integer,
                carga_horaria      integer,
                ativacao           date,
                desativacao        date,
                nome               varchar(280),
                ementa             text)
  LANGUAGE SQL
AS $$
  SELECT d
  FROM public.disciplinas as d,
       public.grade_optativa as rel
  WHERE rel.id_disciplina = d.id_disciplina AND
  		rel.eletiva= FALSE;
$$;