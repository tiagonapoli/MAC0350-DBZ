--------------------------------------------------------------------------------
--- Insere disciplinas
--------------------------------------------------------------------------------

INSERT INTO
public.disciplinas(
  codigo_disciplina,
  creditos_aula,
  creditos_trabalho,
  carga_horaria,
  ativacao,
  nome,
  ementa)
VALUES
( 'MAC0101', 4, 0, 60, '2015-01-01', ' Integração na Universidade e na Profissão', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0110', 4, 0, 60, '1998-01-01', 'Introdução à Computação', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0121', 4, 0, 60, '2015-01-01', ' Algoritmos e Estruturas de Dados I', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0350', 4, 0, 60, '2017-06-01', 'Introdução ao Desenvolvimento de Sistemas de Software', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0426', 4, 0, 60, '1998-01-01', 'Sistemas de Bancos de Dados', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0218', 4, 2, 120, '01-01-01', 'Tecnicas de Programacao II', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0332', 4, 2, 120, '01-01-01', 'Engenharia de Software', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0413', 4, 2, 120, '01-01-01', 'Topicos Avancados de Programacao Orientada a Objetos', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0472', 4, 2, 120, '01-01-01', 'Laboratorio de Metodos Ageis', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0439', 4, 2, 120, '01-01-01', 'Laboratorio de Banco de Dados', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0459', 4, 0, 60, '01-01-01', 'Ciencia e Engenharia de Dados', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0444', 4, 0, 60, '2015-01-01', 'Sistemas Baseados em Conhecimento', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0460', 4, 0, 60, '2015-01-01', 'Aprendizagem Computacional: Modelos, Algoritmos e Aplicacoes', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAC0414', 4, 0, 60, '2015-01-01', 'Automatos, Computabilidade e Complexidade', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAE0399', 4, 0, 60, '2015-01-01', 'Analise de Dados e Simulacao', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAE0515', 4, 0, 60, '2015-01-01', 'Introducao a Teoria dos Jogos', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.'),
( 'MAT0359', 4, 0, 60, '2015-01-01', 'Logica', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, seddo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.');
--------------------------------------------------------------------------------
--- Insere pessoas em seus respectivos cargos
--------------------------------------------------------------------------------

INSERT INTO
public.pessoas(nome)
VALUES
( 'Jef'    ),
( 'Décio da Silva Sauro'  ),
( 'Renato' );

INSERT INTO
public.administradores(id_pessoa, nusp_administrador, data_inicio)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  '0000001',
  '2018-01-01'
);

INSERT INTO
public.professores(id_pessoa, nusp_professor, departamento)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  '0000001',
  'Departamento de Ciência da Computação'
);

INSERT INTO
public.alunos(id_pessoa, nusp_aluno, turma_ingresso)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio da Silva Sauro' LIMIT 1),
  '0000002',
  '2015-03-01'
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Renato' LIMIT 1),
  '0000003',
  '2012-03-01'
);

--------------------------------------------------------------------------------
--- Insere trilhas
--------------------------------------------------------------------------------
INSERT INTO
public.trilhas(
               codigo_trilha, 
               nome, 
               descricao, 
               minimo_disciplinas, 
               minimo_modulos)
VALUES
('ia1', 'Inteligencia Artificial', 'Trilha de Introducao a Inteligencia Artificial', 10, 2),
('ss1', 'Sistemas de Software', 'Trilha de Sistemas', 10, 2);

--------------------------------------------------------------------------------
--- Insere modulos
--------------------------------------------------------------------------------

INSERT INTO
public.modulos(
        codigo_modulo,
        minimo_disciplinas,
        id_trilha)
VALUES
('Introducao a IA', 2, 1),
('Teoria de IA', 1, 1),
('Desenvolvimento de Software', 4, 2),
('Banco de Dados', 2, 2);

--------------------------------------------------------------------------------
--- Insere disciplinas na grade optativa
--------------------------------------------------------------------------------
INSERT INTO
public.grade_optativa (
  id_disciplina,
  ano_grade_optativa,
  eletiva)
VALUES
(5, '2018-01-01', true),
(6, '2018-01-01', true),
(7, '2018-01-01', true),
(8, '2018-01-01', true),
(9, '2018-01-01', true),
(10, '2018-01-01', true),
(11, '2018-01-01', true),
(12, '2018-01-01', true),
(13, '2018-01-01', true),
(14, '2018-01-01', true),
(15, '2018-01-01', true),
(16, '2018-01-01', true),
(17, '2018-01-01', true);

--------------------------------------------------------------------------------
--- Insere disciplinas na grade obrigatoria
--------------------------------------------------------------------------------

INSERT INTO
public.grade_obrigatoria (
  id_disciplina,
  ano_grade_obrigatoria)
VALUES
(1, '2018-01-01'),
(2, '2018-01-01'),
(3, '2018-01-01'),
(4, '2018-01-01');
  
--------------------------------------------------------------------------------
--- Insere disciplinas nos modulos
--------------------------------------------------------------------------------
INSERT INTO
public.optativas_compoem_modulos (
  id_disciplina,
  ano_grade_optativa,
  id_modulo)
VALUES
(13, '2018-01-01', 1),
(12, '2018-01-01', 1),
(11, '2018-01-01', 1),
(17, '2018-01-01', 2),
(16, '2018-01-01', 2),
(15, '2018-01-01', 2),
(14, '2018-01-01', 2),
(11, '2018-01-01', 4),
(10, '2018-01-01', 4),
(5, '2018-01-01', 4),
(9, '2018-01-01', 3),
(8, '2018-01-01', 3),
(7, '2018-01-01', 3),
(6, '2018-01-01', 3);

  
--------------------------------------------------------------------------------
--- Cria estrutura de autenticação
--------------------------------------------------------------------------------

INSERT INTO
auth.usuarios(email_usuario, senha, expira)
VALUES
( 'jef@ime.usp.br', '12345', '2019-01-01' ),
( 'decio@ime.usp.br', '12345', '2019-01-01' ),
( 'renatocf@ime.usp.br', '12345', '2019-01-01' );

INSERT INTO
public.pessoas_geram_usuarios(id_pessoa, id_usuario)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br')
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio da Silva Sauro' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'decio@ime.usp.br')
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Renato' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'renatocf@ime.usp.br')
);

INSERT INTO
auth.perfis(nome_perfil)
VALUES
( 'administrador' ), -- 1
( 'professor' ), -- 2
( 'aluno' ); -- 3

INSERT INTO
auth.servicos(caminho_servico)
VALUES
( '/'             ), -- 1
( '/index'        ), -- 2
( '/estrutura'    ), -- 3
( '/plano'        ), -- 4
( '/alunos'        ), -- 5
( '/alunos/index'  ), -- 6
( '/alunos/new'    ), -- 7
( '/alunos/edit'   ), -- 8
( '/alunos/show'   ); -- 9

INSERT INTO
auth.usuarios_possuem_perfis(id_usuario, id_perfil, expira)
VALUES
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'decio@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'renatocf@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  '2019-01-01'
);

INSERT INTO
auth.perfis_acessam_servicos(id_perfil, id_servico, expira)
VALUES
-- Administrador
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/alunos'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/alunos/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/alunos/new'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/alunos/edit'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/alunos/show'),
  '2019-01-01'
),
-- Professor
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
),
-- Estudante
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
);


----------------------------------
---- Inserir oferecimentos      --
----------------------------------
INSERT INTO
public.professores_oferecem_disciplinas (
  id_pessoa,
  nusp_professor,
  id_disciplina,
  ano_semestre)
VALUES
(1, '0000001', 4, 2015),
(1, '0000001', 5, 2015);


----------------------------------
---- Inserir disciplinas feitas --
----------------------------------
INSERT INTO
public.alunos_cursam_disciplinas (
  id_pessoa,
  nusp_aluno,
  id_oferecimento,
  nota)
VALUES
(2, '0000002', 1, null),
(2, '0000002', 2, 9);

----------------------------------
---- Inserir prerrequisitos --
----------------------------------
INSERT INTO
public.prerequisitos (
  id_disciplina    ,
  id_prerequisito  ,
  total            )
VALUES
(6, 3, true),
(5, 3, true),
(5, 4, true),
(7, 4, true),
(8, 3, true);




