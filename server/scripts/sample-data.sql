--------------------------------------------------------------------------------
--- Insere disciplinas
--------------------------------------------------------------------------------

INSERT INTO
public.disciplinas(
  codigo_disciplina,
  creditos_aula,
  creditos_trabalho,
  carga_horaria,
  ativacao)
VALUES
( 'MAC0101', 4, 0, 60, '2015-01-01' ),
( 'MAC0110', 4, 0, 60, '1998-01-01' ),
( 'MAC0121', 4, 0, 60, '2015-01-01' ),
( 'MAC0350', 4, 0, 60, '2017-06-01' ),
( 'MAC0426', 4, 0, 60, '1998-01-01' );

--------------------------------------------------------------------------------
--- Insere pessoas em seus respectivos cargos
--------------------------------------------------------------------------------

INSERT INTO
public.pessoas(nome)
VALUES
( 'Jef'    ),
( 'Décio'  ),
( 'Renato' );

INSERT INTO
public.administradores(id_pessoa, nusp_administrador, data_inicio)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  '0000001',
  '2018-01-01'
);

INSERT INTO
public.professores(id_pessoa, nusp_professor, departamento)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  '0000001',
  'Departamento de Ciência da Computação'
);

INSERT INTO
public.alunos(id_pessoa, nusp_aluno, turma_ingresso)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
  '0000002',
  '2015-03-01'
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Renato' LIMIT 1),
  '0000003',
  '2012-03-01'
);

--------------------------------------------------------------------------------
--- Insere trilhas
--------------------------------------------------------------------------------
INSERT INTO
public.trilhas(id_trilha,
               codigo_trilha, 
               nome, 
               descricao, 
               minimo_disciplinas, 
               minimo_modulos)
VALUES
(0,'a1', 'Trilha1', 'Esta eh a trilha 1', 1, 2),
(1,'b2', 'Trilha2', 'Esta eh a trilha 2', 2, 3),
(2,'c3', 'Trilha3', 'Esta eh a trilha 3', 99, 2);

--------------------------------------------------------------------------------
--- Insere modulos
--------------------------------------------------------------------------------

INSERT INTO
public.modulos(id_modulo,
        codigo_modulo,
        minimo_disciplinas,
        id_trilha)
VALUES
(0,'otimizacao', 1, 0),
(1,'trolagem', 1, 0),
(2,'futebol', 2, 1),
(3,'malandrops', 2, 2);

--------------------------------------------------------------------------------
--- Insere disciplinas na grade
--------------------------------------------------------------------------------
INSERT INTO
public.grade_optativa (
  id_disciplina,
  ano_grade_optativa,
  eletiva)
VALUES
(1, '2018-01-01', true),
(2, '2018-01-01', false),
(3, '2018-01-01', false),
(4, '2018-01-01', false),
(5, '2018-01-01', false);
--------------------------------------------------------------------------------
--- Insere disciplinas nos modulos
--------------------------------------------------------------------------------
INSERT INTO
public.optativas_compoem_modulos (
  id_disciplina,
  ano_grade_optativa,
  id_modulo)
VALUES
(1, '2018-01-01', 3),
(2, '2018-01-01', 3),
(5, '2018-01-01', 1),
(2, '2018-01-01', 0),
(3, '2018-01-01', 2),
(4, '2018-01-01', 1);

  
--------------------------------------------------------------------------------
--- Cria estrutura de autenticação
--------------------------------------------------------------------------------

INSERT INTO
auth.usuarios(email_usuario, senha, expira)
VALUES
( 'jef@ime.usp.br', '12345', '2019-01-01' ),
( 'decio@ime.usp.br', '12345', '2019-01-01' ),
( 'renatocf@ime.usp.br', '12345', '2019-01-01' );

INSERT INTO
public.pessoas_geram_usuarios(id_pessoa, id_usuario)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br')
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'decio@ime.usp.br')
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Renato' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'renatocf@ime.usp.br')
);

INSERT INTO
auth.perfis(nome_perfil)
VALUES
( 'administrador' ), -- 1
( 'professor' ), -- 2
( 'aluno' ); -- 3

INSERT INTO
auth.servicos(caminho_servico)
VALUES
( '/'             ), -- 1
( '/index'        ), -- 2
( '/estrutura'    ), -- 3
( '/plano'        ), -- 4
( '/alunos'        ), -- 5
( '/alunos/index'  ), -- 6
( '/alunos/new'    ), -- 7
( '/alunos/edit'   ), -- 8
( '/alunos/show'   ); -- 9

INSERT INTO
auth.usuarios_possuem_perfis(id_usuario, id_perfil, expira)
VALUES
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'decio@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'renatocf@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  '2019-01-01'
);

INSERT INTO
auth.perfis_acessam_servicos(id_perfil, id_servico, expira)
VALUES
-- Administrador
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/alunos'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/alunos/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/alunos/new'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/alunos/edit'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/alunos/show'),
  '2019-01-01'
),
-- Professor
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
),
-- Estudante
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
);
